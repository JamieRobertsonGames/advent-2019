import pytest
import src.hello_world as hw


def test_hello_world():
    assert hw.hello_world() == "Hello World"
