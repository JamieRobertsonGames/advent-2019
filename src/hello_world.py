# Imports here


def hello_world():
    """returns back a string to announce to the world

    Returns:
        str: "Hello World" is always returned
    """
    return "Hello World"


def main():
    """entry point for the program
    """
    hello_world()


if __name__ == "__main__":
    main()
